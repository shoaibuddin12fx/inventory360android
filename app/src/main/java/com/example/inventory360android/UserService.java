package com.example.inventory360android;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface UserService {

   // @GET("/api/login/{username}/{password}")
    ///Login?username=j.malik2694&password=alpha442a

//    @POST("Login?username=j.malik2694&password=alpha442a)"

    @POST("Login/{email}/{password}")
    Call login(@Path("email") String username,
               @Path("password") String password);
//    Call login(
//            @Field("email")String username,
//            @Field("password")String password);
}
