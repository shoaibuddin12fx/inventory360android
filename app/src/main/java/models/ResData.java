package models;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;

public class ResData {

    @SerializedName("Status")
    public Integer Status;

    @SerializedName("Data")
    public HashMap Data;

    @SerializedName("PdfUrl")
    public String PdfUrl;

    @SerializedName("Message")
    public String Message;

}